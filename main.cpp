#include "asset.hpp"
#include "parse.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <vector>
#include <Eigen/Dense>
#include <iomanip>
#include <unistd.h>

using namespace std;
using namespace Eigen;

int main(int argc,char** argv){
  if((argc!=3)&&(argc!=4)){
    cerr<<"Number of arguments incorrect!"<<endl;
    return EXIT_FAILURE;
  }

  int opt;
  bool flag=false;
  while ((opt=getopt(argc,argv,"r"))!=-1){
    switch(opt){
      case 'r':
        flag=true;
        break;
      case '?':
        cerr<<"Incorrect option!"<<endl;
        return EXIT_FAILURE;
    }
  }

  //cout<<"check1"<<endl;
  int n=0;
  Portfolio P;
  if(flag==true){
    P.A=readasset(argv[2],n);
    P.corr=readcorr(argv[3],n);
  }
  else{
    P.A=readasset(argv[1],n);
    P.corr=readcorr(argv[2],n);
  }
  P.n=n;
  //cout<<"check2"<<endl;
  cout<<"ROR,volatility"<<endl;
  cout<<fixed;
  if(flag==true){
    for(double rp=0.01;rp<=0.261;rp=rp+0.01){
      cout<<setprecision(1)<<rp*100<<"%, ";
      cout<<setprecision(2)<<P.restrictedOpt(rp)*100<<"%"<<endl;
    }
  }
  else{
    for(double rp=0.01;rp<=0.261;rp=rp+0.01){
      cout<<setprecision(1)<<rp*100<<"%, ";
      cout<<setprecision(2)<<P.unrestrictedOpt(rp)*100<<"%"<<endl;
    }
  }
  return EXIT_SUCCESS;
}
