#ifndef __PARSE_HPP__
#define __PARSE_HPP__

#include "asset.hpp"
#include "portfolio.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

void parse(string& s,vector<string>& v);
vector<Asset> readasset(char* file,int& n);
MatrixXd readcorr(char* file,int n);

#endif
