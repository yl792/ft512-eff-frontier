
#ifndef __ASSET_HPP__
#define __ASSET_HPP__

#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>

using namespace std;

class Asset{
public:
  string name;
  double ror;
  double std;
};

#endif
