#include "asset.hpp"
#include "portfolio.hpp"
#include "parse.hpp"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

void parse(string& s,vector<string>& v){
  string::size_type pos1, pos2;
  pos2=s.find(",");
  pos1=0;
  while(pos2!=string::npos){
    v.push_back(s.substr(pos1, pos2-pos1));
    pos1=pos2+1;
    pos2=s.find(",",pos1);
  }
  if(pos1!=s.length()){
    v.push_back(s.substr(pos1));
    //cout<<s.substr(pos1)<<endl;
  }
}
vector<Asset> readasset(char* file,int& n){
  vector<Asset> A;
  ifstream f(file);
  if(!f.is_open()){
    cerr<<"Cannot find the asset file!"<<endl;
    exit(EXIT_FAILURE);
  }

  string line;
  while(getline(f,line)){
    vector<string> data;
    parse(line,data);
    if(data.size()!=3){
      cerr<<"Format incorrect!"<<endl;
      exit(EXIT_FAILURE);
    }

    Asset temp;
    temp.name=data[0];
    if((atof(data[1].c_str())==0)||(atof(data[2].c_str()))==0){
      cerr<<"Non-numeric value in the asset data!"<<endl;
      exit(EXIT_FAILURE);
    }

    temp.ror=atof(data[1].c_str());
    temp.std=atof(data[2].c_str());
    //cout<<temp.std<<endl;
    A.push_back(temp);

    n++;
  }
  if(n==0){
    cerr<<"Asset data is empty"<<endl;
    exit(EXIT_FAILURE);
  }
  return A;
}

MatrixXd readcorr(char* file,int n){
  ifstream f(file);
  if(!f.is_open()){
    cerr<<"Cannot find the correlation file!"<<endl;
    exit(EXIT_FAILURE);
  }

  MatrixXd corr(n,n);
  string line;
  int i=0;
  while(getline(f,line)){
    if(i==n){
      cerr<<"Dimension of correlation matrix is larger than the asset!"<<endl;
      exit(EXIT_FAILURE);
    }
    //cout<<i<<endl;
    vector<string> data;
    parse(line,data);

    if(data.size()!=(size_t)n){
      cerr<<"Dimension of correlation matrix incorrect!"<<endl;
      exit(EXIT_FAILURE);
    }

    for(int j=0;j<n;j++){
      if(atof(data[j].c_str())==0){
        cerr<<"Non-numeric value in the correlation data!"<<endl;
        exit(EXIT_FAILURE);
      }
      corr(i,j)=atof(data[j].c_str());
      //cout<<corr(i,j)<<" "<<data[j].c_str()<<endl;
    }
    i++;
  }

  if(i==0){
    cerr<<"Correlation file is empty"<<endl;
    exit(EXIT_FAILURE);
  }
  if(i<n){
    cerr<<"Dimension of correlation matrix is smaller than the asset!"<<endl;
    exit(EXIT_FAILURE);
  }

  for(int i=0;i<n;i++){
    for(int j=0;j<n;j++){
      if((corr(i,j)!=corr(j,i))||(corr(i,j)>1)||(corr(i,j)<-1)){
        cerr<<"Incorrect format of the correlation matrix!"<<endl;
        exit(EXIT_FAILURE);
      }
    }

  }
  return corr;
}
