#ifndef __PORTFOLIO_HPP__
#define __PORTFOLIO_HPP__

#include "asset.hpp"
#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <iostream>
#include <vector>
#include <Eigen/Dense>

using namespace std;
using namespace Eigen;

class Portfolio{
public:
  int n;
  vector<Asset> A;
  VectorXd weight;
  double ror;
  double vol;
  MatrixXd corr;

public:
  void calculatevol(){
    double count=0;
    for(int i=0;i<n;i++){
      for(int j=0;j<n;j++){
        //cout<<weight(i)<<" "<<weight(j)<<" "<<A[i].std<<" "<<A[j].std<<" "<<corr(i,j)<<endl;
        count=count+weight(i)*weight(j)*A[i].std*A[j].std*corr(i,j);
      }
      //cout<<count<<endl;
    }
    vol=sqrt(count);
  }

  double unrestrictedOpt(double rp){
    //cout<<"bp"<<endl;
    MatrixXd A_unr_1=MatrixXd::Ones(1,n);
    MatrixXd A_unr_2(1,n);
    for(int i=0;i<n;i++){
      A_unr_2(i)=A[i].ror;
    }
    MatrixXd A_unr(2,n);
    A_unr<<A_unr_1,A_unr_2;

    // cout<<"bp1"<<endl;
    MatrixXd O_unr=MatrixXd::Zero(2,2);
    MatrixXd cov(n,n);
    for(int i=0;i<n;i++){
      for(int j=0;j<n;j++){
        //cout<<corr(i,j)<<endl;
        cov(i,j)=A[i].std*A[j].std*corr(i,j);
        //cout<<A[i].std<<" "<<A[j].std<<" "<<corr(i,j)<<endl;
        //cout<<cov(i,j)<<endl;
      }
    }
    MatrixXd K_unr(n+2,n+2);
    K_unr<<cov,A_unr.transpose(),A_unr,O_unr;
    MatrixXd b_unr_1=MatrixXd::Zero(n,1);
    MatrixXd b_unr_2(2,1);
    b_unr_2<<1,rp;
    //cout<<"bp2"<<endl;
    MatrixXd b_unr(n+2,1);
    b_unr<<b_unr_1,b_unr_2;

    VectorXd x_unr(n+2);
    x_unr=K_unr.fullPivHouseholderQr().solve(b_unr);
    weight=x_unr.head(n);
    ror=rp;
    calculatevol();
    //cout<<"bq3"<<endl;
    return vol;
  }

  double restrictedOpt(double rp){
    MatrixXd A_unr_1=MatrixXd::Ones(1,n);
    MatrixXd A_unr_2(1,n);
    for(int i=0;i<n;i++){
      A_unr_2(i)=A[i].ror;
    }
    MatrixXd A_unr(2,n);
    A_unr<<A_unr_1,A_unr_2;

    MatrixXd O_unr=MatrixXd::Zero(2,2);
    MatrixXd cov(n,n);
    for(int i=0;i<n;i++){
      for(int j=0;j<n;j++){
        cov(i,j)=A[i].std*A[j].std*corr(i,j);
      }
    }
    MatrixXd K_unr(n+2,n+2);
    K_unr<<cov,A_unr.transpose(),A_unr,O_unr;
    MatrixXd b_unr_1=MatrixXd::Zero(n,1);
    MatrixXd b_unr_2(2,1);
    b_unr_2<<1,rp;

    MatrixXd b_unr(n+2,1);
    b_unr<<b_unr_1,b_unr_2;

    VectorXd x_unr(n+2);
    x_unr=K_unr.fullPivHouseholderQr().solve(b_unr);


    MatrixXd A_r=A_unr;
    MatrixXd b_r=b_unr;
    VectorXd x_r=x_unr;
    for(int i=1;;i++){
      int flag=1;
      MatrixXd C;
      int k=0;
      for(int j=0;j<n;j++){
        if(x_r(j)<0){
          MatrixXd t=MatrixXd::Zero(1,n);
          t(0,j)=1;
          MatrixXd temp=C;
          C.resize(k+1,n);
          if(k==0){
            C<<t;
          }
          else{
            C<<temp,t;
          }
          flag=0;
          k++;
        }
      }
      if(flag==1){
        break;
      }
      MatrixXd T;
      T=A_r;
      A_r.resize(T.rows()+C.rows(),n);
      A_r<<T,C;
      T=b_r;
      b_r.resize(T.rows()+C.rows(),1);
      b_r<<T,MatrixXd::Zero(C.rows(),1);

      MatrixXd O_r=MatrixXd::Zero(A_r.rows(),A_r.rows());
      MatrixXd K_r(cov.rows()+A_r.rows(),cov.rows()+A_r.rows());
      K_r<<cov,A_r.transpose(),A_r,O_r;

      x_r=K_r.fullPivHouseholderQr().solve(b_r);
    }
    weight=x_r.head(n);
    ror=rp;
    calculatevol();
    return vol;
  }

};

#endif
